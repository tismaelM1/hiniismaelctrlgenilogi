<?php 

class IntegerAndRoman {
    public function __construct(

      public array $arrayC = [
            'M' => 1000, 
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1
      ],

    ) {}

      public static function convertNumberToRoman(int $number)
      {
            $numberNumValue = intval($number);
            $convertedNumber = '';
            // tableau de corresondance roman to integer
            $romans = array(
                'M' => 1000, 
                'CM' => 900,
                'D' => 500,
                'CD' => 400,
                'C' => 100,
                'XC' => 90,
                'L' => 50,
                'XL' => 40,
                'X' => 10,
                'IX' => 9,
                'V' => 5,
                'IV' => 4,
                'I' => 1);
                
            foreach ($romans as $roman => $romanValue) 
            {
                  $matchingNumber = intval($numberNumValue / $romanValue); // Déterminer le nombre de correspondances
                  $convertedNumber .= str_repeat($roman, $matchingNumber); // si matching number vaut 0 suite a la conversion rien ne se passera.
                  $numberNumValue = $numberNumValue % $romanValue; // Soustraire cela du nombre en parametre pour continuer trkl la boucle de correspondance array conversion
            }
            



            return $convertedNumber;

        }
        public  static function RomanToInt ($roman) {
   
            $romans = array(
                  'M' => 1000,
                  'CM' => 900,
                  'D' => 500,
                  'CD' => 400,
                  'C' => 100,
                  'XC' => 90,
                  'L' => 50,
                  'XL' => 40,
                  'X' => 10,
                  'IX' => 9,
                  'V' => 5,
                  'IV' => 4,
                  'I' => 1,
              );
              
              $result = 0;
            //   on boucle sur le array de correspondance 
              foreach ($romans as $key => $value) {
                    //on boucle sur le string entrer en parmetre 
                  while (strpos($roman, $key) === 0) {
                        // si correspondance dans la string entrer en parametre alors incrementer la valeur de la correspondance avec le array
                      $result += $value;
                      // pour la boucle d'apres on soustrait la chaine qui correspondait car valeur deja incrementer 
                      $roman = substr($roman, strlen($key));
                  }
              }
              return $result;

        }

      // public function debit(int $amount): BankAccount
      // {
      //       if (!$amount > $this->balance) {
      //             // throw new Exception("vous ne pouvez pas etre debiter compte bien en decouvert");
      //             // throw new \RuntimeException('vous ne pouvez pas etre debiter compte bien en decouvert');
      //             return $this;
      //       }
      //       else {
      //             $this->balance -= $amount;
      //       }
  
      //     return $this;
      // }

  }