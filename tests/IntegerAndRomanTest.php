<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class IntegerAndRomanTest extends TestCase
{
    public function testConvertedSimple(): void
    {
        $roman = IntegerAndRoman::convertNumberToRoman(0);
        $this->assertEquals($roman, "");
        $roman = IntegerAndRoman::convertNumberToRoman(5);
        $this->assertEquals($roman, "V");
        $roman = IntegerAndRoman::convertNumberToRoman(10);
        $this->assertEquals($roman, "X");
    }

    public function testConvertedLevel2(): void
    {
        $roman = IntegerAndRoman::convertNumberToRoman(3);
        $this->assertEquals($roman, "III");
        $roman = IntegerAndRoman::convertNumberToRoman(4);
        $this->assertEquals($roman, "IV");
    }

    public function testConvertedMultipleCharRoman(): void
    {
        $roman = IntegerAndRoman::convertNumberToRoman(2678);
        $this->assertEquals($roman, "MMDCLXXVIII");
        $roman = IntegerAndRoman::convertNumberToRoman(1454);
        $this->assertEquals($roman, "MCDLIV");
    }

    //partie 2 

    public function testRomanToIntSimple(): void
    {
        $roman = IntegerAndRoman::RomanToInt("");
        $this->assertEquals($roman, 0);

        $roman = IntegerAndRoman::RomanToInt("V");
        $this->assertEquals($roman, 5);
        $roman = IntegerAndRoman::RomanToInt("X");
        $this->assertEquals($roman, 10);

        $roman = IntegerAndRoman::RomanToInt("IV");
        $this->assertEquals($roman, 4);
        $roman = IntegerAndRoman::RomanToInt("IX");
        $this->assertEquals($roman, 9);
    }

    public function testRomanToIntLevel2(): void
    {
        $roman = IntegerAndRoman::RomanToInt("IV");
        $this->assertEquals($roman, 4);
        $roman = IntegerAndRoman::RomanToInt("IX");
        $this->assertEquals($roman, 9);

        $roman = IntegerAndRoman::RomanToInt("XIII");
        $this->assertEquals($roman, 13);
    }

    public function testRomanToIntLongString(): void
    {
        $roman = IntegerAndRoman::RomanToInt("MMDCLXXVIII");
        $this->assertEquals($roman, 2678);
        $roman = IntegerAndRoman::RomanToInt("MCDLIV");
        $this->assertEquals($roman, 1454);

        $roman = IntegerAndRoman::RomanToInt("MMM");
        $this->assertEquals($roman, 3000);

    }

}
